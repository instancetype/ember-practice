App = Ember.Application.create()

App.Router.map(function() {
  this.route('cats')
  this.route('tennis')
  this.route('about')
  this.route('people')
  this.route('computed')
  this.route('form')
})

// Form
App.FormController = Ember.Controller.extend({
  actions : {
    handleSubmit : function() {
      alert('You typed ' + "'" + this.get('userInput') + "'")
    }
  }
})

// Computed
App.ComputedController = Ember.Controller.extend({
  first : 2
, second : 5
, product : function() {
    return this.get('first') * this.get('second')
  }.property('first', 'second')
})

// ember-data
App.PersonAdapter = DS.FixtureAdapter.extend({})

var attr = DS.attr
App.Person = DS.Model.extend({
  firstName : attr()
, lastName : attr()
, age : attr('number')
})

App.Person.FIXTURES = [
  { firstName : 'Roger'
  , lastName : 'Federer'
  , age : 33
  , id : 1
  }
, { firstName : 'Rafael'
  , lastName : 'Nadal'
  , age : 28
  , id : 2
  }
]

App.PeopleRoute = Ember.Route.extend({
  model : function() {
    return this.store.find('person')
  }
})


// Tennis
App.TennisController = Ember.Controller.extend({
  firstName : 'Roger'
, lastName : 'Federer'
, stats : {
    sport : 'tennis'
  , titles : 17
  }
})


// Cats
App.CatsRoute = Ember.Route.extend({
  model : function() {
    var cats = ['Kalymba', 'Jitsu', 'Sydney Bean']
    return cats
  }
})

App.CatsController = Ember.ArrayController.extend({
  actions : {
    showAlert: function(cat) {
      var catName = cat
      alert('You clicked on ' + catName + ' !')
    }
  , addCat: function() {
      this.get('model').unshiftObject(this.get('name'))
      this.set('name', this.placeholder)
    }
  }
})


// Index
App.IndexRoute = Ember.Route.extend({
  redirect: function() {
    this.transitionTo('about')
  }
})