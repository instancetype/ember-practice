/**
 * Created by instancetype on 9/6/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
App = Ember.Application.create()

App.Router.map(function() {
  this.route('blog')
  this.route('article', { path : 'articles/:article_id' })
})

App.ArticleAdapter = DS.FixtureAdapter.extend()
App.CommentAdapter = DS.FixtureAdapter.extend()

App.Article = DS.Model.extend({
  title : DS.attr()
, body : DS.attr()
, comments : DS.hasMany('comment', { async : true })
})

App.Comment = DS.Model.extend({
  text : DS.attr()
, article : DS.belongsTo('article')
})

App.Article.FIXTURES = [
  { id : 1
  , title : 'My First Post'
  , body : 'This is just some stuff that I\'m writing about in my first post.'
  , comments : [1, 3]
  }
, { id : 2
  , title : 'My Second Post'
  , body : 'I would most likely talk about cats at some point during this one.'
  , comments : [2]
  }
]

App.Comment.FIXTURES = [
  { id : 1
  , text : 'That is not very exciting or informative.'
  , article : 1
  }
, { id : 2
  , text : 'Haha, I would probably do the same!'
  , article : 2
  }
, { id : 3
  , text : 'My Aunt Sally made $600,000.00 working for google last week.'
  , article : 1
  }
]

App.BlogRoute = Ember.Route.extend({
  model : function() {
    return this.store.find('article')
  }
})